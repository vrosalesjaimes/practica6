#!/usr/bin/env python3

from getpass import getpass
from base64 import b64encode
from SecureString import clearmem
from Crypto.Protocol.KDF import PBKDF2
from Crypto.Hash import SHA512
from Crypto.Random import get_random_bytes
from Crypto.Cipher import AES
import MySQLdb
import config
import sys


def encrypt_data(data, key, nonce):
    aes = AES.new(key, AES.MODE_CTR, nonce=nonce)
    ciphertext = aes.encrypt(data)
    return b64encode(ciphertext)


mydb = None

try:
    mydb = MySQLdb.connect(user=config.user, password=config.password, database=config.dbname)
    cursor = mydb.cursor()

    password = getpass()

    with open("diagnosticos_tratamientos.txt", "r") as file:
        data = eval(file.read())
        key = None
        passwordSalt = None

        for record in data.values():
            name = record['name']
            diagnosis = bytes(record['diagnosis'], 'utf-8')
            treatment = bytes(record['treatment'], 'utf-8')

            passwordSalt = get_random_bytes(16)

            key = PBKDF2(password, passwordSalt, 32, count=1000000, hmac_hash_module=SHA512)

            # Generamos nonces aleatorios para cada registro
            diagnosis_nonce = get_random_bytes(8)
            treatment_nonce = get_random_bytes(8)

            diagnosis_ciphertext = encrypt_data(diagnosis, key, diagnosis_nonce)
            treatment_ciphertext = encrypt_data(treatment, key, treatment_nonce)

            passwordSalt_b64 = b64encode(passwordSalt)
            diagnosis_nonce_b64 = b64encode(diagnosis_nonce)
            treatment_nonce_b64 = b64encode(treatment_nonce)

            insert_query = """INSERT INTO expediente (nombre, diagnostico, tratamiento, passwordSalt, diag_nonce, treat_nonce) 
                              VALUES (%s,%s,%s,%s,%s,%s)"""
            record_to_insert = (name, diagnosis_ciphertext, treatment_ciphertext, passwordSalt_b64, diagnosis_nonce_b64,
                                treatment_nonce_b64)
            cursor.execute(insert_query, record_to_insert)

        mydb.commit()
        print("Registros insertados correctamente.")
        clearmem(key)
        clearmem(passwordSalt)

    clearmem(password)

except Exception as err:
    print(f"\nAlgo salió mal: {err}")
    sys.exit()

finally:
    if mydb:
        cursor.close()
        mydb.close()
        print("Conexión con la base de datos cerrada.")

clearmem(password)
