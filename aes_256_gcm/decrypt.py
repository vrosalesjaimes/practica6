#!/usr/bin/env python3

from getpass import getpass
from SecureString import clearmem
from Crypto.Protocol.KDF import scrypt
from Crypto.Cipher import AES
import MySQLdb
import config

def decrypt_data(ciphertext, key, nonce):
    cipher = AES.new(key, AES.MODE_GCM, nonce=nonce)
    decrypted_data = cipher.decrypt(ciphertext)
    return decrypted_data.decode('utf-8')

def get_patient_info(patient_name):
    mydb = None
    password = getpass("Introduce la contraseña:")
    try:
        mydb = MySQLdb.connect(user=config.user, password=config.password, database=config.dbname)
        cursor = mydb.cursor()

        query = "SELECT diagnostico, tratamiento, passwordSalt, diag_nonce, treat_nonce FROM expediente WHERE nombre = %s"
        cursor.execute(query, (patient_name,))
        result = cursor.fetchone()

        if result:
            diagnosis_ciphertext, treatment_ciphertext, passwordSalt, diag_nonce, treat_nonce = result
            key = scrypt(password, salt=passwordSalt, key_len=32, N=16384, r=8, p=1)
            diagnosis = decrypt_data(diagnosis_ciphertext, key, diag_nonce)
            treatment = decrypt_data(treatment_ciphertext, key, treat_nonce)

            clearmem(key)
            clearmem(passwordSalt)
            clearmem(password)
            return diagnosis, treatment
        else:
            clearmem(password)
            return None, None
        
    except Exception as err:
        print(f"Error al obtener la información del paciente: {err}")
        return None, None

    finally:
        if mydb:
            cursor.close()
            mydb.close()
        clearmem(password)

def main():
    patient_name = input("Introduce el nombre del paciente: ")

    if not patient_name.strip():
        print("Por favor, introduce un nombre válido.")
        return

    diagnosis, treatment = get_patient_info(patient_name)

    if diagnosis is not None and treatment is not None:
        print(f"{patient_name}:")
        print(f"\tDiagnóstico para {patient_name}: {diagnosis}")
        print(f"\tTratamiento para {patient_name}: {treatment}")
    else:
        print(f"No se encontraron registros para el paciente {patient_name}.")

if __name__ == "__main__":
    main()
