#!/usr/bin/env python3

from getpass import getpass
from base64 import b64encode
from Crypto.Protocol.KDF import scrypt
from SecureString import clearmem
from Crypto.Cipher import AES
from Crypto.Random import get_random_bytes
import MySQLdb
import config
import sys

def encrypt_data(data, key, nonce):
    cipher = AES.new(key, AES.MODE_GCM, nonce=nonce)
    ciphertext, tag = cipher.encrypt_and_digest(data)
    return ciphertext, cipher.nonce

mydb = None

try:
    mydb = MySQLdb.connect(user=config.user, password=config.password, database=config.dbname)
    cursor = mydb.cursor()

    password = getpass()

    with open("diagnosticos_tratamientos.txt", "r") as file:
        data = eval(file.read())
        key = None
        passwordSalt = None

        for record in data.values():
            name = record['name']
            diagnosis = bytes(record['diagnosis'], 'utf-8')
            treatment = bytes(record['treatment'], 'utf-8')

            passwordSalt = get_random_bytes(16)

            key = scrypt(password, salt=passwordSalt, key_len=32, N=16384, r=8, p=1)

            diag_nonce = get_random_bytes(12)
            treat_nonce = get_random_bytes(12)

            diagnosis_ciphertext, diag_nonce_used = encrypt_data(diagnosis, key, diag_nonce)
            treatment_ciphertext, treat_nonce_used = encrypt_data(treatment, key, treat_nonce)

            cursor.execute("""INSERT INTO expediente (nombre, diagnostico, tratamiento, passwordSalt, diag_nonce, treat_nonce)
                              VALUES (%s, %s, %s, %s, %s, %s)""", (name, diagnosis_ciphertext, treatment_ciphertext,
                                                                    passwordSalt, diag_nonce_used, treat_nonce_used))

        mydb.commit()
        print("Registros insertados correctamente.")

        clearmem(key)
        clearmem(passwordSalt)
    
    clearmem(password)

except Exception as err:
    print(f"\nAlgo salió mal: {err}")
    sys.exit()

finally:
    if mydb:
        cursor.close()
        mydb.close()
        print("Conexión con la base de datos cerrada.")
